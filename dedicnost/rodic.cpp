#include "rodic.h"



void rodic::predstav_sa()
{
	std::cout << "volam sa " << this->meno << " " << this->priezvisko << std::endl;
	std::cout << "byvam na " << this->bydlisko << std::endl;
	std::cout << "na ucte " << this->cislo_uctu << " mam " << this->stav_uctu << " penazi" << std::endl;
}

rodic::rodic()
{
	std::cout << "vytvara sa rodic" << std::endl;
	this->meno = "meno";
	this->priezvisko = "priezvisko";
	this->bydlisko = "bydlisko";
	this->cislo_uctu = "468486486";
	this->stav_uctu = 0;
}

rodic::rodic(std::string meno, std::string priezvisko, std::string bydlisko, std::string nazov_u, int stav)
{
	std::cout << "vytvara sa rodic" << std::endl;
	this->meno = meno;
	this->priezvisko = priezvisko;
	this->bydlisko = bydlisko;
	this->cislo_uctu = nazov_u;
	this->stav_uctu = stav;
}
