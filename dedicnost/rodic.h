#pragma once
#include <string>
#include <iostream>
class rodic
{
private:
	int stav_uctu;
protected:
	std::string meno, priezvisko, bydlisko, cislo_uctu;
public:
	virtual void predstav_sa();
	rodic();
	rodic(std::string meno, std::string priezvisko, std::string bydlisko, std::string nazov_u, int stav);
	virtual ~rodic() { std::cout << "zanika rodic" << std::endl; };
};

