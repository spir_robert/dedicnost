// dedicnost.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "rodic.h"
#include "dieta.h"

int main()
{
	rodic rod("Peter", "Fazula", "bratislava", "SK3456789", 5000);
	rod.predstav_sa();
	dieta syn("janko", "fazula", "bratislava", "SK3456789", 5000,"peter","fazula");
	syn.predstav_sa();
	std::cout << "=============================" << std::endl;
	rodic *pole[2];
	pole[0] = &rod;
	pole[1] = &syn;
	for (int i = 0; i < 2; i++)
	{
		pole[i]->predstav_sa();
	}
	std::cout << "=============================" << std::endl;
	pole[0] = new rodic("Peter", "Fazula", "bratislava", "SK3456789", 5000);
	pole[1] = new dieta("janko", "fazula", "bratislava", "SK3456789", 5000, "peter", "fazula");
	for (int i = 0; i < 2; i++)
	{
		delete pole[i];
	}
	std::cout << "=============================" << std::endl;
	rozhranie *roz;
	roz = &syn;
	roz->funkcia_rozhrania();
    return 0;
}

